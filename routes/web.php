<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\SettingController::class, 'index']);
Route::post('/', [App\Http\Controllers\SettingController::class, 'create'])->name('settings.create');

Route::get('/inventory', [App\Http\Controllers\InventoryController::class, 'index'])->name('inventory.index');
Route::post('/inventory', [App\Http\Controllers\InventoryController::class, 'edit'])->name('inventory.edit');
Route::get('/reset', [App\Http\Controllers\InventoryController::class, 'reset'])->name('inventory.reset');