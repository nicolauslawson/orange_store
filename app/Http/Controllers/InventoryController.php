<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Inventory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\AddSettingsFormRequest;

class InventoryController extends Controller
{
    public function index(Request $request) {
        $data = Inventory::whereNotNull('shelf')->get();
        return view('inventory.index', compact('data'));
    }
    public function edit(Request $request) {
        $set = Setting::first();
        $item = Inventory::where('shelf', 1)->first();

        if ($item->total >= $request->number) {
            $count = $item->total - $request->number;
            $item->total = $count;
            if ($count == 0) {
                $item->status = null;
                $item->shelf = null;
                $item->save();

                $sort = Inventory::where('total', '!=', 0)->get();

                for ($j=0; $j<$sort->count(); $j++) {
                    $newItem = Inventory::find($sort[$j]->id);
                    if ($newItem->shelf == 2) {
                        $newItem->shelf = 1;
                        $newItem->status = 'Priority';
                    }
                    if ($newItem->shelf == 3) {
                        $newItem->shelf = 2;
                    }
                    if ($j == 2) {
                        $newItem->shelf = 3;
                    }
                    $newItem->save();
                }
                if ($sort->count() < 3) {
                    $addItem = new Inventory;
                    $addItem->container_name = 'Container-'.Str::random(5);
                    $addItem->total = $set->number_orange;
                    $addItem->type = 'Apple';
                    $addItem->shelf = 3;
                    $addItem->save();
                    return redirect()->back()->with('message', 'Transaction complete. No more fruit in container.<br> Next container moved to shelf 1 and others reassigned <br> Container with apples added to shelf 3');
                }
                return redirect()->back()->with('message', 'Transaction complete. No more fruit in container.<br> Next container moved to shelf 1 and others reassigned: $'.$request->number * $set->price_orange);
            }
            $item->save();
            return redirect()->back()->with('message', 'Transaction complete. Please review shelf below.<br> Total collected: $'.$request->number * $set->price_orange);
        }
    }
    public function reset(Request $request) {
        Setting::truncate();
        Inventory::truncate();
        return view('index');
    }
}
