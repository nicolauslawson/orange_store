<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Inventory;
use Illuminate\Http\Request;
use App\Http\Requests\AddSettingsFormRequest;

class SettingController extends Controller
{
    public function index() {
        return view('index');
    }
    public function create(AddSettingsFormRequest $request) {
        $set = new Setting;
        $set->containers = $request->container;
        $set->number_orange = $request->number_orange;
        $set->price_orange = $request->price_orange;
        $set->save();

        for ($i = 1; $i<=$request->container; $i++) {
            $item = new Inventory;
            $item->container_name = 'Container'.$i;
            $item->total = $request->number_orange;
            $item->type = 'Orange';
            if ($i <= 3) {
                $item->shelf = $i;
            }
            if ($i == 1) {
                $item->status = 'Priority';
            }
            $item->save();
        }
        return redirect()->route("inventory.index");
    }
}
