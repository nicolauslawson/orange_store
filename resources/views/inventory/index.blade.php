@extends('templates.default')
@section('content')
<h1 class="mt-4">Inventory <a role="button" class="btn btn-sm btn-danger" href="{{ route('inventory.reset') }}">Reset Application</a></h1>
<hr style="border: 2px solid black;">
  @if(session()->has('message'))
      <div class="alert alert-success">
          {!! session()->get('message') !!}
      </div>
  @endif
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Count</th>
        <th scope="col">Shelf</th>
        <th scope="col">Type</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
        @foreach ($data as $element)
            <tr>
                <td>{{$element->container_name}}</td>
                <td>{{$element->total}}</td>
                <td>{{$element->shelf}}</td>
                <td>{{$element->type}}</td>
                <td><b>{{$element->status}}</b></td>
            </tr>
        @endforeach
    </tbody>
  </table>
  <hr style="border: 2px solid black;">
  <form action="{{ route('inventory.edit') }}" method="post">
    <input type="hidden" name="number" value="1" class="form-control">
    <button type="submit" class="btn btn-primary mt-2">Sell Item</button>
    {{ csrf_field() }}
</form>
@endsection