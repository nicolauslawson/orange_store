@extends('templates.default')
@section('content')
<h1 class="text-center mt-4">Welcome to the orange store</h1>
<hr style="border: 2px solid black;">

@if($errors->any())
    <div class="alert alert-danger" role="alert">
        {!! implode('', $errors->all('<div>:message</div>')) !!}
    </div>
@endif
<form action="{{ route('settings.create') }}" method="post">
    <div class="form-row">
        <div class="form-group col-6">
            <label for="">Number of containers</label>
            <input type="text" name="container" class="form-control">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-6">
            <label for="">Number of oranges</label>
            <input type="text" name="number_orange" class="form-control">
        </div>
    </div>
    <div class="form-row">
        <div class=" form-group col-6">
            <label for="">Price per oranage</label>
            <input type="text" name="price_orange" class="form-control">
        </div>
    </div>
    <button type="submit" class="btn btn-primary mt-2">Add Settings</button>
    {{ csrf_field() }}
</form>
@endsection